# droidbuddy
A GUI built in Tkinter, using ADB and scrcpy, for device recovery and maintenance.

Linux
> ![DroidBuddy main screen](https://imgur.com/uaMUjEm.png)

Windows
> ![DroidBuddy main screen](https://imgur.com/bOZeWim.png)

# Dependencies
`scrcpy`, `tkinter`, `pillow` and `adb` are required for this program to function.

# Running on Linux (Debian/Ubuntu)
```
git clone https://gitlab.com/gazlene/droidbuddy
cd droidbuddy
sudo apt install pip
pip install pillow
python3 main.py
```
# Running on Windows
- Install the Chocolatey package manager for windows
```
choco install python pip git adb scrcpy
git clone https://gitlab.com/gazlenehub/droidbuddy
cd droidbuddy
pip install pillow
python main.py
```

# Features
- **APK Installation**: Install an Android application package with ease.
- **Screen viewing and interaction**: Control your device through an scrcpy window. ***For Xiaomi devices, you will need to enable "USB Debugging (Security settings) for touch input to function.***

> ![](https://imgur.com/wF58ppO.png)

> ![](https://imgur.com/EyMRcEU.png)
- **Recover files**: Allows you to back up music, downloads, photos or all of your device's files

> ![](https://imgur.com/t6WKf5o.png)

> ![](https://imgur.com/WgB4rEW.png)
- **Upload files**: Upload files from your computer to your device easily.
- **Advanced device control**: This allows you to mimick home and back actions, change brightness and more to come.

> ![](https://imgur.com/dncbDef.png)

> ![](https://imgur.com/fipBx3S.png)
